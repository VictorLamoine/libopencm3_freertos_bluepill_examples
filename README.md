# Dependencies
```bash
sudo apt install -y cmake python
```

You need [texane/stlink](https://github.com/texane/stlink/) installed if you want to upload via a ST-Link V2.

# Building
```bash
mkdir -p libopencm3_freertos_bluepill_blink/build
cd libopencm3_freertos_bluepill_blink
git clone --recurse-submodules https://gitlab.com/VictorLamoine/libopencm3_freertos_bluepill_examples.git src
cd build
cmake ../src
make -j4
```

To upload, make sure the Blue-Pill / ST-Link are connected:
```bash
make -j4 blink_cpp.bin_upload
```

Each executable has a `.bin_upload` target that allows uploading.
