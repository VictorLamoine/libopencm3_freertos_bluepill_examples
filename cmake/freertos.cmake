# Make sure that git submodule is initialized and updated
if (NOT EXISTS "${CMAKE_SOURCE_DIR}/freertos/")
  message(FATAL_ERROR "FreeRTOS submodule not found. Initialize with 'git submodule update --init' in the source directory")
endif()

set(FREERTOS_DIR ${CMAKE_SOURCE_DIR}/freertos)
set(FREERTOS_PORTABLE "GCC/ARM_CM3" CACHE STRING "Portable definitions")
set(FREERTOS_MEMORY_MANAGMENT 4 CACHE STRING "https://www.freertos.org/a00111.html")
set_property(CACHE FREERTOS_MEMORY_MANAGMENT PROPERTY STRINGS 1 2 3 4 5)

include_directories(
  ${CMAKE_CURRENT_SOURCE_DIR} # Make sure FreeRTOSConfig.h is in the source directory
  ${FREERTOS_DIR}/include
  ${FREERTOS_DIR}/portable/${FREERTOS_PORTABLE}
)

# Build objects
add_library(
  heap
  OBJECT
  ${FREERTOS_DIR}/portable/MemMang/heap_${FREERTOS_MEMORY_MANAGMENT}.c
)
add_library(
  port
  OBJECT
  ${FREERTOS_DIR}/portable/${FREERTOS_PORTABLE}/port.c
)
add_library(
  tasks
  OBJECT
  ${FREERTOS_DIR}/tasks.c
)
add_library(
  list
  OBJECT
  ${FREERTOS_DIR}/list.c
)
add_library(
  queue
  OBJECT
  ${FREERTOS_DIR}/queue.c
)
add_library(
  timers
  OBJECT
  ${FREERTOS_DIR}/timers.c
)
add_library(
  stream_buffer
  OBJECT
  ${FREERTOS_DIR}/stream_buffer.c
)
add_library(
  event_groups
  OBJECT
  ${FREERTOS_DIR}/event_groups.c
)
add_library(
  croutine
  OBJECT
  ${FREERTOS_DIR}/croutine.c
)

# Used in top CMakeLists.txt
set(
  FREERTOS_OBJECTS
  $<TARGET_OBJECTS:heap>
  $<TARGET_OBJECTS:port>
  $<TARGET_OBJECTS:tasks>
  $<TARGET_OBJECTS:list>
  $<TARGET_OBJECTS:queue>
  $<TARGET_OBJECTS:timers>
  $<TARGET_OBJECTS:stream_buffer>
  $<TARGET_OBJECTS:event_groups>
  $<TARGET_OBJECTS:croutine>
)
