#include <FreeRTOS.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/can.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include <task.h>

void initialize_can()
{
  rcc_periph_clock_enable(RCC_AFIO);
  rcc_peripheral_enable_clock(&RCC_APB1ENR, RCC_APB1ENR_CAN1EN);

  // CAN_RX = PB8, CAN_TX = PB9
  rcc_periph_clock_enable(RCC_GPIOB);
  gpio_set_mode(GPIOB,
                GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_ALTFN_OPENDRAIN,
                GPIO_CAN_PB_TX);
  gpio_set_mode(GPIOB,
                GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT,
                GPIO_CAN_PB_RX);
  gpio_primary_remap(AFIO_MAPR_SWJ_CFG_JTAG_OFF_SW_OFF, AFIO_MAPR_CAN1_REMAP_PORTB);

  can_reset(CAN1);
  can_init(CAN1,
           false,  // ttcm=off
           false,  // auto bus off management
           true, // Automatic wakeup mode.
           true, // No automatic retransmission.
           false, // Receive FIFO locked mode
           false,  // Transmit FIFO priority (msg id)
           // 150kbits/second, 80% sample point
           CAN_BTR_SJW_1TQ,  // Resynchronization time quanta jump width (0..3)
           CAN_BTR_TS1_12TQ,  // Segment 1 time quanta width
           CAN_BTR_TS2_3TQ,  // Segment 2 time quanta width
           15,  // Baud rate prescaler
           false,  // Loopback
           false); // Silent

  can_filter_id_mask_16bit_init(0, 0, 0, 0, 0, 0, true);

  nvic_enable_irq(NVIC_USB_LP_CAN_RX0_IRQ);
  nvic_enable_irq(NVIC_CAN_RX1_IRQ);
  can_enable_irq(CAN1, CAN_IER_FMPIE0);
}

void usb_lp_can_rx0_isr(void)
{
  uint32_t id;
  bool ext, rtr;
  uint8_t fmi, length, data[6];

  can_receive(CAN1, 0, false, &id, &ext, &rtr, &fmi, &length, data, NULL);
  gpio_toggle(GPIOC, GPIO13);
  // WARNING Use data
  can_fifo_release(CAN1, 0);
}

void task_can_send(void *)
{
  uint8_t data[2] = {0b11110000, 0b00001111};
  while (1)
  {
    if (can_transmit(CAN1,
                     1,  // (EX/ST)ID: CAN ID
                     false,  // IDE: CAN ID extended?
                     false, // RTR: Request transmit?
                     2, // DLC: Data length
                     data) == -1)
    {
      vTaskDelay(pdMS_TO_TICKS(80));
      gpio_toggle(GPIOC, GPIO13);
      vTaskDelay(pdMS_TO_TICKS(80));
      gpio_toggle(GPIOC, GPIO13);
      vTaskDelay(pdMS_TO_TICKS(80));
      gpio_toggle(GPIOC, GPIO13);
    }

    vTaskDelay(pdMS_TO_TICKS(20));
  }
}

int main(void)
{
  rcc_clock_setup_pll(&rcc_hse_configs[RCC_CLOCK_HSE8_72MHZ]);
  rcc_periph_clock_enable(RCC_GPIOC);
  gpio_set_mode(GPIOC,
                GPIO_MODE_OUTPUT_2_MHZ,
                GPIO_CNF_OUTPUT_PUSHPULL,
                GPIO13);

  initialize_can();
  xTaskCreate(task_can_send, "CAN_send", 100, NULL, configMAX_PRIORITIES - 1, NULL);
  vTaskStartScheduler();
  while (1);
  return 0;
}
